msgid ""
msgstr ""
"Project-Id-Version: rkhunter 1.3.0-3\n"
"Report-Msgid-Bugs-To: Source: rkhunter@packages.debian.org\n"
"POT-Creation-Date: 2007-12-12 01:13+0530\n"
"PO-Revision-Date: 2007-11-27 20:10+0200\n"
"Last-Translator: Esko Arajärvi <edu@iki.fi>\n"
"Language-Team: Finnish <debian-l10n-finnish@lists.debian.org>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-Language: Finnish\n"
"X-Poedit-Country: Finland\n"

#. Type: boolean
#. Description
#: ../templates:2001
msgid "Activate daily run of rkhunter?"
msgstr "Aktivoidaanko rkhunterin päivittäinen ajo?"

#. Type: boolean
#. Description
#: ../templates:2001
msgid ""
"If you choose this option, rkhunter will be run automatically by a daily "
"cron job."
msgstr ""
"Jos valitset tämän vaihtoehdon, rkhunter ajetaan päivittäin automaattisesti "
"cron-työnä."

#. Type: boolean
#. Description
#: ../templates:3001
msgid "Activate weekly update of rkhunter's databases?"
msgstr "Aktivoidaanko rkhunterin tietokantojen viikottainen päivitys?"

#. Type: boolean
#. Description
#: ../templates:3001
msgid ""
"If you choose this option, rkhunter databases will be updated automatically "
"by a weekly cron job."
msgstr ""
"Jos valitset tämän vaihtoehdon, rkhunterin tietokannat päivitetään "
"automaattisesti viikottain cron-työnä."

#. Type: boolean
#. Description
#: ../templates:4001
msgid "Automatically update rkhunter's file properties database?"
msgstr ""
"Päivitetäänkö rkhunterin tiedosto-ominaisuustietokanta automaattisesti?"

#. Type: boolean
#. Description
#: ../templates:4001
msgid ""
"The file properties database can be updated automatically by the package "
"management system."
msgstr ""
"Tiedosto-ominaisuustietokanta voidaan päivittää automaattisesti "
"pakettienhallintajärjestelmän avulla."

#. Type: boolean
#. Description
#: ../templates:4001
msgid ""
"This feature is not enabled by default as database updates may be slow on "
"low-end machines. Even if it is enabled, the database won't be updated if "
"the 'hashes' test is disabled in rkhunter configuration."
msgstr ""
"Tämä ominaisuus ei ole oletuksena aktivoitu, koska tietokantapäivitykset "
"saattavat olla hitaita tehottomilla koneilla. Vaikka tämä olisi aktivoituna, "
"tietokantoja ei päivitetä, jos 'hashes'-testi on passivoitu rkhunterin "
"asetuksissa."
